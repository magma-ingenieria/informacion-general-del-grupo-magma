# Documentos necesarios

<br>

## Documentos necesarios para el contrato en GEDOCO

Portal Gestión de Documentación para Contratación Universidad del Magdalena GEDOCO [enlace](https://gedoco.unimagdalena.edu.co)

- **Fotocopia del acta o diploma de grado:** *Descargar o escanear diploma o acta de grado.*
- **Hoja de vida SIGEP:** *Descargar la hoja de vida generada desde la página del [SIGEP II](https://www.funcionpublica.gov.co/sigep-web/sigep2/index.xhtml?faces-redirect=true#no-back-button).*
- **RUT:** [Descargar aquí.](https://muisca.dian.gov.co/WebArquitectura/DefLoginOld.faces)
- **Certificado procudaduría:** [Descargar aquí.](https://www.procuraduria.gov.co/Pages/Generacion-de-antecedentes.aspx)
- **Certificado contraloría:** [Descargar aquí.](https://www.contraloria.gov.co/web/guest/persona-natural)
- **Certificado antecedentes de la Policía:** [Descargar aquí.](https://antecedentes.policia.gov.co:7005/WebJudicial/)
- **Medidas correctivas de la Policía:** [Descargar aquí.](https://srvcnpc.policia.gov.co/PSC/frm_cnp_consulta.aspx)
- **Tarjeta o matrícula profesional:** *Descargar desde la entidad generadora de este: Por ejemplo, El consejo profesional nacional de ingenierías eléctrica mecánica y profesiones afines.*
- **Certificado de salud:** *Descargar de su EPS.*
- **Certificado de pensión:** *Descargar de su AFP.*
- **Cédula de ciudadanía:** *Escanear cédula cara y cara al 150% en formato PDF.*
- **Exámen médico ocupacional:** *Ir a un entidad prestadora del servicio de examenes médicos y escanear el resultado.*
- **Declaración de innabilidades e incompatibilidades:** *Se descarga desde la misma página del GEDOCO.*
- **Declaración juramentada y autorización de manejo de datos:** *Se descarga desde la misma página del GEDOCO.*
- **Carta retefuente:** *Se descarga desde la misma página del GEDOCO.*
- **Última planilla de pago:** *Descargar de su gestor: Por ejemplo, aportes en línea o PILA.*
- **Certificado REDAM:** *Se descarga desde la misma página del GEDOCO.*
- **Certificación bancaria:** *Descargar desde su banco.*
- **Fotocopia del acta o diploma de posgrado:** *Descargar o escanear diploma o acta de posgrado si aplica.*
- **Carnet de vacunación COVID-19:** *Descargar o escanear carnet o certificado de vacunación de las dosis aplicadas contra COVID-19 o descargar desde la misma página del GEDOCO certificado de exoneración en caso de no contar con la vacuna.*
- **Acta de verificación:** *Acta de aprobación de todos los documentos cargados en la plataforma GEDOCO. Este documento es de validación interna, es decir que, es subido por el personal que lleva a cabo el proceso de contratación.*

<br>

## Documentos necesarios para solicitar los honorarios

- **Doc_01_cuenta_de_cobro:** [Descargar aquí.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/raw/main/contratistas/formatos_cuentas_de_cobro/Doc_01_cuenta_de_cobro.docx?inline=false)
- **Doc_02_informe_acta_ordenes:** [Descargar aquí.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/raw/main/contratistas/formatos_cuentas_de_cobro/Doc_02_Informe_acta_Ordenes.xlsx?inline=false)
- **Doc_03_planilla_de_pago:** *Descargar de su gestor: Por ejemplo, aportes en línea o PILA.*
- **Doc_04_ADRES:** [Descargar aquí.](https://www.adres.gov.co/consulte-su-eps) 
- **Doc_05_certificado_salud:** *Descargar de su EPS.*
- **Doc_06_certificado_pension:** *Descargar de su AFP.*
- **Doc_07_carta_retefuente** [Descargar aquí.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/raw/main/contratistas/formatos_cuentas_de_cobro/Doc_07_carta_retefuente.docx?inline=false)
- **Doc_08_informe_actividades:** [Descargar aquí.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/raw/main/contratistas/formatos_cuentas_de_cobro/Doc_08_Informe_actividades.doc?ref_type=heads&inline=false)
- **Doc_09_anexos:** *Formato libre, es importante evidenciar que se cumplen las actividades que están plasmadas en la OPSP.*

<br>

## Importante:
- Enviar al supervisor los documentos que requieren su firma en formato editable.
- Los formatos o las versiones de los mismos pueden cambiar intempestivamente, debe estar atento a las comunicaciones emitidas por el profesional de la vicerrectoría a cargo de su proceso. 
