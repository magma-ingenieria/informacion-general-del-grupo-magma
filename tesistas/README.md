# Documentos para Tesistas MAGMA Ingenieria

<br>

## Modalidad de grado artículo científico
- **Infografía:** [Ver infografía.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/blob/main/tesistas/modalidad_de_grado_articulo_cientifico/infografia_articulo_cientifico.pdf)
- **Formato entrega del artículo científico - Consejo de Programa:** [Descargar aquí.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/raw/main/tesistas/modalidad_de_grado_articulo_cientifico/formato_entrega_documentos_consejo_de_programa.docx?inline=false)

<br>

## Modalidad de grado trabajo de investigación
- **Infografía:** [Ver infografía.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/blob/main/tesistas/modalidad_de_grado_trabajo_de_investigacion/infografia_trabajo_de_investigacion.pdf)
- **Formato entrega documentos Consejo de Programa:** [Descargar aquí.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/raw/main/tesistas/modalidad_de_grado_trabajo_de_investigacion/formato_entrega_documentos_consejo_de_programa.docx?inline=false)
- **Formato modelo carta aval trabajo de grado:** [Descargar aquí.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/raw/main/tesistas/modalidad_de_grado_trabajo_de_investigacion/formato_modelo_carta_aval_trabajo_de_grado.docx?inline=false)
- **Formato presentación de propuestas:** [Descargar aquí.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/raw/main/tesistas/modalidad_de_grado_trabajo_de_investigacion/formato_presentacion_de_propuesta.docx?inline=false)
- **Formato de modificaciones a la propuesta de trabajo de grado:** [Descargar aquí.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/raw/main/tesistas/modalidad_de_grado_trabajo_de_investigacion/formato_de_modificaciones_a_la_propuesta_de_trabajo_de_grado.docx?inline=false)
- **Formato presentación informe final:** [Descargar aquí.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/raw/main/tesistas/modalidad_de_grado_trabajo_de_investigacion/formato_presentacion_informe_final.docx?inline=false)
- **Formato presentación informe final versión 2:** [Descargar aquí.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/raw/main/tesistas/modalidad_de_grado_trabajo_de_investigacion/Formato%20de%20Presentaci%C3%B3n%20Propuesto%20Informe%20Final.docx?inline=false)
