# Semilleristas en MAGMA

- **Ser estudiante activo.**
- **Copia del documento de identidad.**
- **Certificado de afiliación a una EPS.**
- **Diligenciar el formato de registro:** [Descargar aquí.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/raw/main/semilleristas/formato_de_registro_a_semilleros.docx?inline=false)
