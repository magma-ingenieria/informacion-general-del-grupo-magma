# Ayudantes, Auxiliares y Asistentes de Investigación

<br>

## Documentos necesarios para la vinculación

<br>

### Ayudantes de investigación

- **Certificado de cuenta bancaria:** Descargar desde su aplicación de banco.
- **Copia del documento de identidad:** Escanear documento de identidad cara/cara y convertir al formato PDF.
- **Captura de pantalla del módulo estudiantil [AyRE](https://admisiones.unimagdalena.edu.co/mEstudiantes/indexPrinc.jsp?rnd=127904075):** Para confirmar que se encuentra como estudiante activo matriculado.
- **Declaración juramentada para ayudantes:** [Descargar aquí.](/ayudantes/formatos_para_ayudantes/Declaración%20Juramentada%20Ayudantes.docx)

<br>

### Asistentes de investigación

- **Copia del documento de identidad:** Escanear documento de identidad cara/cara y convertir al formato PDF.
- **Captura de pantalla del módulo estudiantil [AyRE](https://admisiones.unimagdalena.edu.co/mEstudiantes/indexPrinc.jsp?rnd=127904075):** Para confirmar que se encuentra como estudiante activo matriculado.
- **Certificado de cuenta bancaria:** Descargar desde su aplicación de banco.

<br>

## Documentos necesarios para el cobro

<br>

### Ayudantes de investigación

- **Carta remisoria para el pago:** Carta dirigida a la Vicerrectoría de Investigación solicitando el pago de las horas, en donde se especifique el número de horas trabajadas, el monto a cobrar y el periodo de actividades. Debidamente firmada por el/la supervisor/a.
- **Formato de actividades para ayudantes:** [Descargar aquí.](/ayudantes/formatos_para_ayudantes/Formato%20de%20Reporte%20de%20Actividades.docx)
- **Captura de pantalla del módulo estudiantil [AyRE](https://admisiones.unimagdalena.edu.co/mEstudiantes/indexPrinc.jsp?rnd=127904075):** Para confirmar que se encuentra como estudiante activo todavia matriculado.
- **Certificado de cuenta bancaria:** Descargar desde su aplicación de banco.

<br>

### Asistentes de investigación

- **Carta remisoria para el pago:** Carta dirigida a la Vicerrectoría de Investigación solicitando el pago parcial, en donde se especifique el periodo de actividades y el monto a cobrar. Debidamente firmada por el/la supervisor/a.
- **Certificado de cuenta bancaria:** Descargar desde su aplicación de banco.
- **Captura de pantalla del módulo estudiantil [AyRE](https://admisiones.unimagdalena.edu.co/mEstudiantes/indexPrinc.jsp?rnd=127904075):** Para confirmar que se encuentra como estudiante activo todavia matriculado.
- **Formato de informe de actividades:** [Descargar aquí.](/ayudantes/formatos_para_asistentes/Formato_Nuevo_Informe_Actividades.doc)